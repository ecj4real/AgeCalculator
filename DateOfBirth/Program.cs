﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateOfBirth
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the Date of Birth in number format dd|mm|yyyy");
            string input = Convert.ToString(Console.ReadLine());

            string[] splitted = input.Split('|');
            int year = Convert.ToInt32(splitted[2]);
            int month = Convert.ToInt32(splitted[1]);
            int day = Convert.ToInt32(splitted[0]);
            try
            {
                DateTime birthDate = new DateTime(year, month, day);

                DateTime currentDate = DateTime.Today;

                int age = 0;

                if (birthDate.Year > currentDate.Year)
                {
                    Console.WriteLine("invalid year input");
                }
                else
                {
                    if (birthDate.Month < currentDate.Month || (birthDate.Month == currentDate.Month && birthDate.Day <= currentDate.Day))
                    {
                        age = currentDate.Year - birthDate.Year;
                    }
                    else
                    {
                        age = currentDate.Year - birthDate.Year - 1;
                    }

                    Console.WriteLine("You are {0} year(s) old", age);
                }
            }
            catch (Exception)
            {

                Console.WriteLine("Invalid Date of Birth");
                Console.WriteLine("Press any key to continue . . .");
                Console.ReadKey();
                Environment.Exit(0);
            };

            
            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey();
        }
    }
}
